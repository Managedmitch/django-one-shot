from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    todolist_list = TodoList.objects.all()
    context = {"todo_object": todolist_list}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        "detail_object": detail,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            create = form.save()
            return redirect("todo_list_detail", id=create.id)
    else:
        form = TodoListForm()

    context = {"form": form}
    return render(request, "todos/create.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            create = form.save()
            return redirect("todo_list_detail", id=create.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "items/create_items.html", context)


def todo_item_update(request, id):
    update_item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=update_item)
        if form.is_valid:
            update_item = form.save()
            return redirect("todo_list_detail", id=update_item.id)
    else:
        form = TodoItemForm(instance=update_item)
    context = {"form": form}
    return render(request, "items/edit.html", context)


def todo_list_update(request, id):
    todolist_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist_list)
        if form.is_valid():
            todolist_list = form.save()
            return redirect("todo_list_detail", id=todolist_list.id)
    else:
        form = TodoListForm(instance=todolist_list)
    context = {
        "todo_object": todolist_list,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_list_delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list_delete.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")
