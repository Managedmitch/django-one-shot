from django.contrib import admin
from todos.models import TodoList, TodoItem


@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )


@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    def is_completed(self):
        if self.is_completed == "Completed":
            return True
        return False

    list_display = (
        "task",
        "due_date",
    )
